﻿using UnityEngine;

// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class ExampleClass : MonoBehaviour
{
    CharacterController characterController;

    private float speed = 6.0f;
    private float jumpSpeed = 10.0f;
    public float gravity = 20.0f;
    public float jumpDistance = 5;
    private bool Jumping = false;

    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (characterController.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes


            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0);
            moveDirection *= speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection = new Vector3(0, 0.0f, 0);

                moveDirection.y = jumpSpeed;

                if (Input.GetButton("Horizontal"))
                {
                    //saltar horizontal

                    moveDirection.x = Input.GetAxisRaw("Horizontal") * jumpDistance;




                }
                Jumping = true;
            }



        }
        else
        {
            if (Input.GetButtonDown("Jump") && Jumping)
            {


                moveDirection.y = jumpSpeed;
                moveDirection.y /= 1.2f;
                moveDirection.x = Input.GetAxisRaw("Horizontal") * jumpDistance;

                Jumping = false;

            }

        }
        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);
    }
}
