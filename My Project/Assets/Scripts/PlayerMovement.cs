﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    private float speed = 5f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    Vector3 velocity;
    bool isGrounded;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -4f;
        }

        if (isGrounded)

        {
            speed = 10f;
            float x = Input.GetAxis("Horizontal");
            Vector3 move = transform.right * x;

            controller.Move(move * speed * Time.deltaTime);
        }


        if (Input.GetButton("Jump") && isGrounded)
        {
            float x = Input.GetAxis("Horizontal") * 1;
            Vector3 move = transform.right * x;
            controller.Move(move * Time.deltaTime);
            velocity.y = Mathf.Sqrt(jumpHeight * -4f * gravity);
        }


        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * 2f * Time.deltaTime);


    }
}
